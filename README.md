Los Vengadores
*Greysha Lora 
*Francis Castillo
*Abimael Alvarez
*Luis Steuth 

----RESTAURANTE EL VEGANO------

Descripcion:
El proyecto a desarrollar orientado a un restaurante VEGANO el cual llevara el nombre el "EL VEGANO"
donde presentaremos una web para ventas de comida orientada al tipo de restaurante seleccionado donde 
ofreceremos servicios de reservaciones, pedido a domicilio, servios de bufette y organizacion de fiestas 
relacionadas a la comida vegana. 

a traves de la pagina  se reaizara publucidad para dar a conocer el estilo de vida y alimentacion vegana,
donde presentaremos  dierentes informaciones para eliminar los tabues que existen al rededor de esta forma 
de alimentacion libre de carnes y derivados animales y poder presentar de igual modo que realmente este 
estilo de vida es implementado por otros continentes siendo los principales el asiatico, el cual usaremos 
como ejemplo, para mostrar como este estilo de alimentacion vasada en vegetales puede permitir gran logividad 
y salud muy estable. 



--------------------------------------------------------------------------------------------------------------
                                             ESTRUCTURA DE LA PAGINA
--------------------------------------------------------------------------------------------------------------


La pagina principal estara compuesta por un cabecera, cuerpo y submenues, y pies de pagina. 

En la cabecera llevara el logo al lado izquierdo y al lado derecho llevara un menu para navegación dentro de la pagina.
este menú estará intregrado por botones (Inicio, Menús, Servicios, Reservaciones, Blog, Cotacto).

En el cuerpo estaremos desarrollado diferentes contenedores con información, imagenes acerca del servicio a brindar, menús 
den entrada, nuestro equipo de trabajo y post del blog. 

En el pies de pagina estableceremos información como los de la redes sociales, dirección con un pequeño mapa, telefonos y 
correo electronico, derechos de autor o copyright. 


Nuestras sección de Menús y Servicios brindara un catalogo con información e imagenes, categorizada que sea entendible
por los visitantes de la web.

La sección de Reservaciones, dispondra de un formulario para que el cliente registre un futura visita y pedira 
todas las informaciones necesarias para registrar a dicho usuario y reservar dicho espacio. 

la seccion de Blog, para mantener al tanto a nuestros clientes con contenido de nuestra empresa y saber sus inquietudes 
y sujerecias acerca de los servicios que se le estan brindando. 

la seccion de contacto, brindar todas las informaciones correspondiente al nuestra empresa tanto de numeros telefonicos, 
correos, redes sociales y un mapa con la ubicacion de nuestro local. 

--------------------------------------------------------------------------------------------------------------
                                             DIVICION DE LA PAGINA POR INTEGRANTAS
--------------------------------------------------------------------------------------------------------------

*Greysha Lora: Contacto y F.A.Q
*Francis Castillo: Menu y Reservas
*Abimael Alvarez: El Blog
*Luis Steuth: Index y Servicios
